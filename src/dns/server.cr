# :nodoc:
class DNS::Server
  def initialize(addr : Socket::IPAddress)
    @udp_socket = UDPSocket.new
    # @tcp_socket = TCPSocket.new
    @udp_socket.bind addr
    # @tcp_socket.bind addr
  end

  def start
    # TODO merge with Message.from_socket (need addr)
    # TODO shut down properly if socket is closed during/after receive
    spawn do
      # next if @udp_socket.closed?
      message = Bytes.new(512)
      bytes_read, client_addr = @udp_socket.receive message
      message = Message.from_io IO::Memory.new(message), IO::ByteFormat::BigEndian

      io = IO::Memory.new
      resolve(message).to_io io, IO::ByteFormat::BigEndian
      @udp_socket.send io.to_slice, client_addr
    end
  end

  def resolve(message : Message) : Message
    message.answers << RR::Record.new name: message.questions[0].name, record_type: RecordType::A, data: IPv4Address.new("1.2.3.4")
    message
  end

  def close
    @udp_socket.close
    # @tcp_socket.close
  end
end
