class DNS::IPv6Address
  private property slice : Bytes

  def initialize(@slice : Bytes)
    raise "bad slice size" unless slice.size == 16
  end

  def initialize(str : String)
    io = IO::Memory.new 16
    str.split(":").each do |byte_str|
      io.write byte_str.hexbytes
    end
    @slice = io.to_slice
  end

  def bytesize
    @slice.size
  end

  def to_s(io : IO)
    @slice.hexstring.scan(/..../).map(&.to_a.first).join(":").to_s(io)
  end

  def to_io(io : IO, format : IO::ByteFormat)
    io.write @slice
  end

  def to_slice
    @slice
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    slice = Bytes.new 16
    io.read slice
    self.new slice
  end
end
