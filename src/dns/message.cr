class DNS::Message
  property header : Header
  property questions : Array(Question)
  property answers : Array(RR::Record)
  property name_servers : Array(RR::Record)
  property additionals : Array(RR::Record)

  def initialize(@header = Header.new,
                 @questions = [] of Question,
                 @answers = [] of RR::Record,
                 @name_servers = [] of RR::Record,
                 @additionals = [] of RR::Record)
  end

  # Adds a question to the message. Note that very few servers support more than one question per message.
  def add_question(name, question_type : RecordType = RecordType::AAAA, question_class = RecordClass::Internet)
    @questions << Question.new name, question_type, question_class
  end

  # Returns `true` if the response code is not an error.
  def ok? : Bool
    @header.ok?
  end

  # Removes all responses and sets header.truncation to true.
  def truncate
    @header.truncation = true
    # TODO this is overkill, instead remove only what exceeds the 512 byte limit
    @answers.clear
    @name_servers.clear
    @additionals.clear
  end

  # Writes message to socket.
  def to_socket(socket : Socket, format : IO::ByteFormat = IO::ByteFormat::BigEndian)
    io = IO::Memory.new

    case socket.protocol
    when Socket::Protocol::TCP
      io.write Bytes.new 2
      to_io io, format
      io.rewind
      format.encode io.size.to_u16 - 2, io
    when Socket::Protocol::UDP
      to_io io, format
      if io.size > 512
        io.clear
        truncate
        to_io io, format
      end
    else
      raise "unsupported protocol #{socket.protocol}"
    end

    socket.write io.to_slice
  end

  # Writes message to io.
  def to_io(io : IO, format : IO::ByteFormat)
    @header.question_count = @questions.size.to_u16
    @header.answer_count = @answers.size.to_u16
    @header.name_server_count = @name_servers.size.to_u16
    @header.additional_count = @additionals.size.to_u16
    @header.to_io(io, format)
    @questions.each &.to_io(io, format)
    @answers.each &.to_io(io, format)
    @name_servers.each &.to_io(io, format)
    @additionals.each &.to_io(io, format)
  end

  # Reads message from socket.
  def self.from_socket(socket : Socket, format : IO::ByteFormat = IO::ByteFormat::BigEndian) : self
    case socket.protocol
    when Socket::Protocol::TCP
      slice = Bytes.new 65535
      bytes_read = socket.read slice
      bytes_remaining = format.decode UInt16, slice[..2]
      bytes_remaining -= bytes_read - 2
      io = IO::Memory.new slice[2..bytes_read]
      loop do
        break if bytes_remaining.zero?
        slice = Bytes.new bytes_remaining
        bytes_read = socket.read slice
        bytes_remaining -= bytes_read
        io.write slice[..bytes_read]
      end
    when Socket::Protocol::UDP
      datagram = Bytes.new 512
      socket.read datagram
      io = IO::Memory.new datagram
    else
      raise "unsupported protocol #{socket.protocol}"
    end

    self.from_io io, format
  end

  # Reads message from io. IO must implement `IO#seek` and `IO#pos`.
  # If you have a `Socket`, use `from_socket` instead.
  def self.from_io(io : IO, format : IO::ByteFormat) : self
    header = Header.from_io io, format
    questions = header.question_count.times.map { Question.from_io(io, format) }.to_a
    answers = header.answer_count.times.map { RR::Record.from_io io, format }.to_a
    name_servers = header.name_server_count.times.map { RR::Record.from_io io, format }.to_a
    additionals = header.additional_count.times.map { RR::Record.from_io io, format }.to_a
    Message.new(
      header: header,
      questions: questions,
      answers: answers,
      name_servers: name_servers,
      additionals: additionals,
    )
  end
end
