class DNS::Resolver
  private property ns : NameServerConfig

  def initialize(@config : Config = Config.cloudflare)
    @ns = @config.name_servers.first # TODO
    case @ns.protocol
    when Socket::Protocol::UDP
      @socket = UDPSocket.new
    when Socket::Protocol::TCP
      @socket = TCPSocket.new
    else
      raise "unsupported protocol #{@ns.protocol}"
    end

    @socket.read_timeout = @config.rw_timeout
    @socket.write_timeout = @config.rw_timeout

    @socket.connect @ns.address
  end

  # Sends a message and retrieves the result.
  def send(message : Message) : Message
    message.to_socket @socket
    response = Message.from_socket @socket

    if response.header.truncation && !@config.disable_tcp_fallback
      tcp = TCPSocket.new
      tcp.connect @ns.address
      message.to_socket tcp
      response = Message.from_socket tcp
    end

    response
  end

  # Sends a query.
  # ```
  # resolver = DNS::Resolver.new
  # pp resolver.query "example.com", RecordType::AAAA
  # resolver.close
  # ```
  def query(name, record_type : RecordType, record_class = RecordClass::Internet) : Message
    header = Header.new recursion_desired: true
    message = Message.new header
    message.add_question name, record_type, record_class
    send message
  end

  # Closes the socket.
  def close
    @socket.close
  end

  # Creates a temporary resolver, sends a query and returns the result. If you are sending more than one
  # query at a time, create your own Resolver instance instead.
  # ```
  # DNS::Resolver.query "example.com", RecordType::AAAA
  # ```
  def self.query(name, record_types : RecordType, record_class = RecordClass::Internet) : Message
    self.new.query name, record_types, record_class
  end
end

class DNS::Resolver::Config
  property name_servers : Array(NameServerConfig)

  # DNS over UDP has a message limit of 512 bytes. When the truncation flag is set, the resolver
  # will retrieve the message over TCP instead. Set this option to disable this.
  property disable_tcp_fallback : Bool

  property rw_timeout : Time::Span

  def initialize(@name_servers, @disable_tcp_fallback = false, @rw_timeout = 5.seconds)
  end

  def self.google
    self.new [
      NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("8.8.8.8", 53)),
      NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("8.8.4.4", 53)),
      NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("2001:4860:4860::8888", 53)),
      NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("2001:4860:4860::8844", 53)),
    ]
  end

  def self.cloudflare
    self.new [
      NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("1.1.1.1", 53)),
      NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("1.0.0.1", 53)),
      NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("2606:4700:4700::1111", 53)),
      NameServerConfig.new(Socket::Protocol::UDP, Socket::IPAddress.new("2606:4700:4700::1001", 53)),
    ]
  end
end

class DNS::Resolver::NameServerConfig
  property protocol : Socket::Protocol
  property address : Socket::Address

  def initialize(@protocol, @address)
  end
end
