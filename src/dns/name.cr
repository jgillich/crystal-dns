class DNS::Name
  private property labels : Array(Bytes)

  def initialize(@labels : Array(Bytes) = [] of Bytes)
  end

  def initialize(name : String)
    @labels = name.split(".").map(&.to_slice)
  end

  def to_s(io : IO)
    @labels.map { |label| String.new label }.join(".").to_s(io)
  end

  def bytesize
    size = 0
    @labels.each do |l|
      size += l.size
    end
    size += 1 # null terminator
    size
  end

  # Writes name as a null terminated string to io.
  def to_io(io : IO, format : IO::ByteFormat)
    @labels.each do |label|
      label.bytesize.to_u8.to_io io, format
      io.write label
    end
    io.write_bytes 0_i8, format
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    return_pos = nil
    labels = [] of Bytes
    loop do
      len = UInt8.from_io io, format
      if len.zero?
        io.seek(return_pos) unless return_pos.nil?
        break
      end
      if len >= 192
        pointer = UInt8.from_io io, format
        return_pos = io.pos if return_pos.nil?
        io.seek(pointer)
      else
        label = Bytes.new len
        io.read label
        labels << label
      end
    end
    self.new labels
  end
end
