class DNS::RR::Record
  property name : Name
  property record_type : RecordType
  property record_class : RecordClass
  property ttl : UInt32
  property data : Data

  def initialize(@name = Name.new,
                 @record_type = RecordType::AAAA,
                 @record_class = RecordClass::Internet,
                 @ttl = 0_u32,
                 @data = Bytes.new 0)
  end

  def to_io(io : IO, format : IO::ByteFormat)
    @name.to_io io, format
    @record_type.value.to_io io, format
    @record_class.value.to_io io, format
    @ttl.to_io io, format

    case data = @data
    when Bytes
      data.size.to_u16.to_io(io, format)
      io.write(data)
    when Name
      data.to_io(io, format)
    when IPv4Address
      4_u16.to_io(io, format)
      data.to_io(io, format)
    when IPv6Address
      16_u16.to_io(io, format)
      data.to_io(io, format)
    when RR::SOA, RR::HINFO, RR::MX
      data.to_io(io, format)
    else
      raise "unexpected record data type"
    end
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    name = Name.from_io io, format
    record_type = RecordType.new(UInt16.from_io(io, format))
    record_class = RecordClass.new(UInt16.from_io(io, format))
    ttl = UInt32.from_io io, format

    data : Data
    {% begin %}
    case record_type
    when RecordType::CNAME, RecordType::ANAME, RecordType::NS, RecordType::PTR
      data = Name.from_io io, format
    {% for r in %w(SOA HINFO MX) %}
    when RecordType::{{r.id}}
      data = {{r.id}}.from_io io, format
    {% end %}
    else
      len = UInt16.from_io io, format
      case record_type
      when RecordType::NULL
        data = Bytes.new len
        io.read data
      else
        case record_class
        when RecordClass::Internet
          case record_type
          when RecordType::A
            data = IPv4Address.from_io io, format
          when RecordType::AAAA
            data = IPv6Address.from_io io, format
          else
            data = Bytes.new len
            io.read data
          end
        else
          data = Bytes.new len
          io.read data
        end
      end
    end
    {% end %}

    Record.new(
      name: name,
      record_type: record_type,
      record_class: record_class,
      ttl: ttl,
      data: data,
    )
  end
end

alias DNS::RR::Data = Name |
                      IPv4Address |
                      IPv6Address |
                      Bytes |
                      SOA |
                      HINFO |
                      MX
