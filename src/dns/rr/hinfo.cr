class DNS::RR::HINFO
  property cpu : String
  property os : String

  def initialize(@cpu = "",
                 @os = "")
  end

  def to_io(io : IO, format : IO::ByteFormat)
    (@cpu.bytesize + 1 + @os.bytesize + 1).to_u16.to_io io, format
    Util.string_to_io(io, format, @cpu)
    Util.string_to_io(io, format, @os)
  end

  def self.from_io(io : IO, format : IO::ByteFormat) : self
    len = UInt16.from_io(io, format)
    self.new(
      cpu: Util.string_from_io(io, format),
      os: Util.string_from_io(io, format),
    )
  end
end
