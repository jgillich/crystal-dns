require "spec"
require "../src/dns"
include DNS

describe Resolver do
  describe "#query" do
    it "resolves A" do
      msg = Resolver.query("www.example.com", RecordType::A)
      msg.ok?.should be_true
      msg.answers.size.should eq 1
      msg.answers[0].record_type.should eq RecordType::A
      msg.answers[0].record_class.should eq RecordClass::Internet
      msg.answers[0].name.to_s.should eq "www.example.com"
      msg.answers[0].data.to_s.should eq "93.184.216.34"
    end

    it "resolves AAAA" do
      msg = Resolver.query("www.example.com", RecordType::AAAA)
      msg.ok?.should be_true
      msg.answers.size.should eq 1
      msg.answers[0].record_type.should eq RecordType::AAAA
      msg.answers[0].record_class.should eq RecordClass::Internet
      msg.answers[0].name.to_s.should eq "www.example.com"
      msg.answers[0].data.to_s.should eq "2606:2800:0220:0001:0248:1893:25c8:1946"
    end

    it "resolves MX" do
      msg = Resolver.query("gmail.com", RecordType::MX)
      msg.ok?.should be_true
      msg.answers.size.should eq 5
      msg.answers[0].record_type.should eq RecordType::MX
      msg.answers[0].record_class.should eq RecordClass::Internet
      msg.answers[0].name.to_s.should eq "gmail.com"
      mx = msg.answers.find { |r| r.data.as(RR::MX).preference == 5 }
      mx.nil?.should be_false
      mx.not_nil!.data.as(RR::MX).exchange.to_s.should eq "gmail-smtp-in.l.google.com"
    end

    it "resolves multiple" do
      resolver = Resolver.new
      10.times do |i|
        msg = resolver.query "10.0.0.#{i}.xip.io", RecordType::A
        msg.ok?.should be_true
        msg.answers.size.should eq 1
        msg.answers[0].name.to_s.should eq "10.0.0.#{i}.xip.io"
        msg.answers[0].data.to_s.should eq "10.0.0.#{i}"
      end
    end

    it "resolves over TCP" do
      config = Resolver::Config.new [Resolver::NameServerConfig.new(Socket::Protocol::TCP, Socket::IPAddress.new("8.8.8.8", 53))]
      resolver = Resolver.new config
      msg = resolver.query("www.example.com", RecordType::A)
      msg.ok?.should be_true
      msg.answers.size.should eq 1
      msg.answers[0].name.to_s.should eq "www.example.com"
      msg.answers[0].data.to_s.should eq "93.184.216.34"
    end

    it "resolves very large responses" do
      config = Resolver::Config.new [Resolver::NameServerConfig.new(Socket::Protocol::TCP, Socket::IPAddress.new("173.255.232.166", 53))]
      resolver = Resolver.new config
      msg = resolver.query("255.count.dnsp.co", RecordType::A)
      msg.ok?.should be_true
      msg.answers.size.should eq 255
      msg.answers.each_with_index do |answer, i|
        answer.data.to_s.should eq "#{i + 1}.#{i + 1}.#{i + 1}.#{i + 1}"
      end
    end

    it "times out instead of blocking" do
      addr = Socket::IPAddress.new("127.0.0.1", 9999)
      socket = UDPSocket.new
      socket.bind addr
      config = Resolver::Config.new [Resolver::NameServerConfig.new(Socket::Protocol::UDP, addr)]
      resolver = Resolver.new config

      spawn do
        message = Bytes.new(512)
        bytes_read, client_addr = socket.receive message
        # don't feel like responding today
      end

      expect_raises(IO::TimeoutError) do
        msg = resolver.query("www.example.com", RecordType::A)
      end
    end
  end
end
